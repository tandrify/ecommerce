const mongoose = require('mongoose');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const {ApolloServer} = require('apollo-server-express');
const {PrismaClient} = require('@prisma/client');

const prisma = new PrismaClient();

const schema = require('./schema');

const url = "mongodb://localhost:27017/test_js_tandrify";
const connect = mongoose.connect(url,
    {useNewUrlParser: true, useUnifiedTopology: true}
);

connect.then((db) => {
    console.log('Connected successfully to database!');
}, (err) => {
    console.log(err);
});

const app = express();
app.use(bodyParser.json());
app.use('*', cors());
app.set('port', process.env.PORT || 5000);
app.listen(app.get('port'), () => {
    console.log(`🚀 Server listening to port ${app.get('port')}`);
});

const server = new ApolloServer({
    typeDefs: schema.typeDefs,
    resolvers: schema.resolvers,
    context: () => {
        return {prisma};
    }
});
server.applyMiddleware({app});
