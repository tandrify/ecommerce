const {gql} = require('apollo-server-express');

const typeDefs = gql`
    type Client {
        id: ID!
        name: String
        email: String
        products: [Product]
    }

    type Product {
        id: ID!
        title: String
        desc: String
        price: String
        clients: [Client]
    }

    type Query {
        getClient(id: Int!): Client
        getClients: [Client]
        getProduct(id: Int!): Product
        getProducts: [Product]
    }

    type Mutation {
        createClient(input: CreateClientInput): Client
        createProduct(input: CreateProductInput): Product
        addToCart(input: CreateProductInput): [Product]
    }

    input CreateClientInput {
        name: String
        email: String
    }

    input CreateProductInput {
        title: String
        desc: String
        price: String
    }

    input AddToCartInput {
        clientId: Int
        productIds: [Int]
    }
`;

const resolvers = {
    Query: {
        getClient: (_, {id}, ctx) => {
            return ctx.prisma.client.findOne({
                where: {id},
            })
        },
        getClients: (_, __, ctx) => {
            return ctx.prisma.client.findMany();
        },
        getProduct: (_, {id}, ctx) => {
            return ctx.prisma.product.findOne({
                where: {id},
            })
        },
        getProducts: (_, __, ctx) => {
            return ctx.prisma.product.findMany();
        },
        // clients: (product, _, ctx) => {
        //     return ctx.prisma.client.findMany({
        //         where: {product: {id: product.id}},
        //     })
        // },
        // products: (client, _, ctx) => {
        //     return ctx.prisma.product.findMany({
        //         where: {client: {id: client.id}},
        //     })
        // },
    },
    Mutation: {
        createClient: (_, {input}, ctx) => {
            const name = input && input.name;
            const email = input && input.email;

            if (!name || !email) return;

            return ctx.prisma.client.create({
                data: {name, email}
            })
        },
        createProduct: (_, {input}, ctx) => {
            const title = input && input.title;
            const desc = input && input.desc;
            const price = input && input.price;

            if (!title || !desc || !price) return;

            return ctx.prisma.product.create({
                data: {title, desc, price}
            })
        },
        addToCart: async (_, {input}, ctx) => {
            const clientId = input && input.clientId;
            const productIds = input && input.productIds;

            if (!clientId || !(productIds && productIds.length)) return;

            const client = await ctx.prisma.client.findOne({
                where: {id: clientId,},
            })

            if (!client) return;

            let newProducts = [];

            for (const productId of productIds) {
                if (!productId) continue;
                const newProduct = await ctx.prisma.product.update({
                    where: {id: productId},
                    data: {
                        connect: {
                            client: {id: client.id},
                        },
                    }
                })
                if (newProduct) newProducts.push(newProduct);
            }
            return newProducts;
        }
    }
};

module.exports = {typeDefs, resolvers};
