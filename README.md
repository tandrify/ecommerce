# Projet e-commerce GraphQL

## Setup
```
cp .env.example .env
Remplacer user et password dans DATABASE_URL
yarn prisma:migrate
yarn prisma:generate
```

## Run
```
yarn start
http://localhost:5000/graphql
```


